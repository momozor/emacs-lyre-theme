# emacs-lyre-theme

An Emacs color theme made with ThemeCreator.

![emacs](https://user-images.githubusercontent.com/24475030/34203059-c31ce9d0-e5b4-11e7-8938-1304bdede31c.png)

## Getting Started

### Installing the theme

1. Launch emacs

2. Hit `Alt+x` and type `package-install-file` and hit `Enter`.

3. Type the path where `lyre-theme.el` located and hit `Enter`.

4. Again, hit `Alt+x` and type `load-theme` and hit `Enter`.

5. Then type `lyre` to load the theme.

## Authors

* [faraco](https://github.com/faraco) <skelic3@gmail.com>
        
## License

This project is licensed under the GPL3 License - see the LICENSE file for details.
    
